<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nroCarnet', 20);
            $table->string('nombreCliente', 50);
            $table->string('direccion', 60)->nullable();
            $table->string('ciudad', 60)->nullable();            
            $table->string('telefono', 20)->nullable();            
            $table->decimal('descuento', 15,2)->nullable();
            $table->decimal('saldo', 15,2)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
