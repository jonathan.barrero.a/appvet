<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('animal_id');
            $table->foreign('animal_id')->references('id')->on('animales');
            $table->date('fechaVisita');
            $table->string('tipoSeguimiento');
            $table->date('fechaSeguimiento');
            $table->string('formaPago')->nullable();
            $table->decimal('totalFactura', 15,2)->nullable();
            $table->decimal('cantidadPagado', 15,2)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitas');
    }
}
