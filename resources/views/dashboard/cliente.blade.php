@extends('layouts.app')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <div class="card-header">
                    Registro de clientes
                </div>
                <div class="card-body">
                    <h5 class="card-title">Special title treatment</h5>
                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
                </div>
            </div>
        </div>          
    <div class="text-center pt-4">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ModalCliente">
            Nuevo Cliente
        </button>    
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="ModalCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria1-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        C.I./NIT:
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="idCi" name="ci" aria-describedby="CarnetHelp" placeholder="Ingrese Nro Identidad">
                    </div>
                </div>
                <div class="col-md-2">
                    Nombre Cliente:
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="txtnombreCliente" name="nombreCliente" aria-describedby="nombreHelp" placeholder="Ingrese el Nombre Completo">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        Direccion:
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="txtDireccion" name="direccion" aria-describedby="direccionHelp" placeholder="(Opcional) Ingrese una direccion">
                    </div>
                    <div class="col-md-2">
                        Ciudad:
                    </div>
                    <div class="col-md-2">
                        <select name="ciudad_id" class="form-control border-input">
                            <option disabled selected>Seleccione Ciudad</option>                            
                        </select>
                    </div>
                    <div class="col-md-2">
                        Telefono:
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="txtTelefono" name="telefono" aria-describedby="TelefonoHelp" placeholder="(Opcional) Ingrese un Telefono">
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
    </div>
    </div>
  </div>
</div>


@endsection