<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class animales extends Model
{
    use SoftDeletes;
    protected $fillable = ['cliente_id','nombreAnimal','tipoAnimal','raza','fechaNacimiento','sexo','esterilizado'];
}
