<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class visitas extends Model
{
    use SoftDeletes;
    protected $fillable = ['animal_id','fechaVisita','tipoSeguimiento','fechaSeguimiento','formaPago','totalFactura','cantidadPagado'];
}
