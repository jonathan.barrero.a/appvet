<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class detalleVisitas extends Model
{
    use SoftDeletes;
    protected $fillable = ['visita_id','detalleVisita'];
}
