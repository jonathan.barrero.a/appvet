<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clientes extends Model
{
    use SoftDeletes;
    protected $fillable = ['nroCarnet','nombreCliente','direccion','ciudad','telefono','descuento','saldo'];
}
